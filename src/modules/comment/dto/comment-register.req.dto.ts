

export interface CommentRegisterRequestDto{
  body: string,
  issueId: number,
  userId: number,
}
